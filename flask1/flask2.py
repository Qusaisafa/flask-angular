import sqlite3

from flask import Flask
from flask import render_template
from flask import request, g
import os
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('mysql://qusai:1994@localhost:3306/fadfid', convert_unicode=True, echo=False)
Base = declarative_base()
Base.metadata.reflect(engine)
from sqlalchemy.orm import relationship, backref


class Users(Base):
    __table__ = Base.metadata.tables['users']


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('login2.html')


@app.route('/myprofile')
def profile():
    return render_template('profile.html')


@app.route('/addprofileform')
def profileform():
    return render_template('login2.html')


@app.route('/add',methods=['POST','GET'])
def add_people():
    myname = request.args.get('myname')
    mystate = request.args.get('mystate')
    from sqlalchemy.orm import scoped_session, sessionmaker, Query

    db_session = scoped_session(sessionmaker(bind=engine))
    session = db_session
    q = session.query(Users). \
        filter(Users.username == myname).first()
    if q.password == mystate:
        return render_template('profile.html', html_page_name=myname)
    else:
        return render_template('hello.html')


@app.route('/Logout')
def logout():
    return render_template('login2.html')


if __name__ == '__main__':
    app.run()
