from flask import Flask
from flask import render_template
from flask import request, g
import os
from sqlalchemy.orm import relationship, backref
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('mysql://qusai:1994@localhost:3306/fadfid', convert_unicode=True, echo=False)
Base = declarative_base()
Base.metadata.reflect(engine)


class Guest(Base):
    __table__ = Base.metadata.tables['guest']

if __name__ == '__main__':
    from sqlalchemy.orm import scoped_session, sessionmaker, Query
    db_session = sessionmaker(bind=engine)
    session = db_session()
    N = Guest(firstname='qusai', lastname='safa')
    session.add(N)
    session.commit()